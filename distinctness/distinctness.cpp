/*
 ELEMENT DISTINCTNESS ALGORITHM
 There are multiple ways of detecting whether the elements are distinct or not.
 One of the ways is to sort the elements in the array and check if the elements next to each other
 are equal or not.
 On the other hand an n^2 loop can be used to select an element from the array and check if the
 the element exists.
 The approach used in this algorithm is adding the elements in an array implementation of a binary tree
 If the elements already exists in the tree then the elements are not distinct else it is. The function
 performing the algorithm is isDistinct function.
*/
#include <iostream>
#include <limits>

#include "../config.h"

using namespace std;

#define SIZE  20
#define MAX numeric_limits<int>::max()


VIP_ENCINT elements1[SIZE] = {25,97,1,10,36,22,74,3,9,12,30,1,63,148,99,13,64,49,80,15}; // is distinct
VIP_ENCINT elements2[SIZE] = {25,97,1,10,36,22,74,3,99,12,30,81,63,148,99,13,64,49,99,15}; // is not distinct. e.g., 99 

VIP_ENCBOOL
isDistinct(VIP_ENCINT elements[], VIP_ENCINT &dup)
{
#ifdef VIP_DO_MODE
	// VIP_ENCINT tree[SIZE][3];
	dup = MAX;
	
	#ifdef VIP_NAIVE_ACCESS	
		// cout<<"VIP_NAIVE_ACCESS"<<endl;
		VIP_ENCINT _tree [SIZE * 3];  // Flattened Version of 2D array
		for(int i = 0; i < SIZE;i++){
			// tree[i][0] = MAX;
			_tree[i * 3 + 0] = MAX;

			// tree[i][1] = MAX;
			_tree[i * 3 + 1] = MAX;

			// tree[i][2] = MAX;
			_tree[i * 3 + 2] = MAX;
		}	
		// tree[0][0] = elements[0];
		_tree[0] = elements[0];
		VIP_ENCINT location = 0;
		VIP_ENCBOOL _result = true;

		for(int i = 1; i < SIZE; i++){
			VIP_ENCINT temp = 0;
			// VIP_ENCINT value = DO_ACCESS_INT(0, 0, 0, tree[0] );
			// VIP_ENCINT left = DO_ACCESS_INT(0, 1, 0, tree[0] );
			// VIP_ENCINT right = DO_ACCESS_INT(0, 2, 0, tree[0] );
			VIP_ENCINT value = DO_ACCESS_INT(0, (0*3) + 0, 0, _tree );
			VIP_ENCINT left = DO_ACCESS_INT(0, (0*3) + 1, 0, _tree );
			VIP_ENCINT right = DO_ACCESS_INT(0, (0*3) + 2, 0, _tree );
			VIP_ENCBOOL isMax_r = false;
			VIP_ENCBOOL isMax_l = false;
			VIP_ENCBOOL isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));

			
			for( int k = 0; k < SIZE/2; k++){
				VIP_ENCBOOL condition1 = elements[i] > value;
				VIP_ENCBOOL condition2 = right != MAX;

				VIP_ENCBOOL _isRightNotMax = condition1 && condition2;
				VIP_ENCBOOL _isRightMax = condition1 && !condition2;

				VIP_ENCBOOL condition3 = elements[i] < value;
				VIP_ENCBOOL condition4 = left != MAX;

				VIP_ENCBOOL _isLeftNotMax = condition3 && condition4;
				VIP_ENCBOOL _isLeftMax = condition3 && !condition4;

				VIP_ENCBOOL notBoth = !condition1 && !condition3;

				// cout<<"isLeftNotMax: "<<VIP_DEC(_isLeftNotMax)<<" isLeftMax: "<<VIP_DEC(_isLeftMax) <<" isRightNotMax: "<<VIP_DEC(_isRightNotMax)<<" isRightMax: "<<VIP_DEC(_isRightMax)<<endl;
				// value = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right, VIP_ENCINT(0), 0, tree ), value);
				// left = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right,  VIP_ENCINT(1), 0, tree ), left);
				// temp = VIP_CMOV(isNotDone && _isRightNotMax, right, temp);
				// right = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right,  VIP_ENCINT(2), 0, tree ), right);

				value = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(0), 0, _tree ), value);
				left = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(1), 0, _tree ), left);
				temp = VIP_CMOV(isNotDone && _isRightNotMax, right, temp);
				right = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(2), 0, _tree ), right);

				// DO_ACCESS_INT2D(1, location+1,  VIP_ENCINT(0), VIP_CMOV(isNotDone && _isRightMax, elements[i], DO_ACCESS_INT2D(0, location+1,  VIP_ENCINT(0), 0, tree )) , tree );
				// DO_ACCESS_INT2D(1, temp,  VIP_ENCINT(2), VIP_CMOV(isNotDone && _isRightMax, location + 1, DO_ACCESS_INT2D(0, temp,  VIP_ENCINT(2), 0, tree )), tree );
				DO_ACCESS_INT(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isRightMax, elements[i], DO_ACCESS_INT(0, ((location+1)*3) +  VIP_ENCINT(0), 0, _tree )) , _tree );
				DO_ACCESS_INT(1, (temp*3) + VIP_ENCINT(2), VIP_CMOV(isNotDone && _isRightMax, location + 1, DO_ACCESS_INT(0, (temp*3) +  VIP_ENCINT(2), 0, _tree )), _tree );
				location = VIP_CMOV(isNotDone && _isRightMax, location + 1, location);
				isMax_r = VIP_CMOV(isNotDone && _isRightMax, VIP_ENCBOOL(true), isMax_r);
				
				// value = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT2D(0, left, VIP_ENCINT(0), 0, tree ), value);
				// right = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT2D(0, left, VIP_ENCINT(2), 0, tree ), right);
				value = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(0), 0, _tree ), value);
				right = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(2), 0, _tree ), right);
				temp = VIP_CMOV(isNotDone && _isLeftNotMax, left, temp);
				left = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(1), 0, _tree ), left);
					
				// DO_ACCESS_INT2D(1, temp, VIP_ENCINT(1), VIP_CMOV(isNotDone && _isLeftMax, location+1, DO_ACCESS_INT2D(0, temp, VIP_ENCINT(1), 0, tree )), tree );
				// DO_ACCESS_INT2D(1, location+1,  VIP_ENCINT(0), VIP_CMOV(isNotDone && _isLeftMax, elements[i], DO_ACCESS_INT2D(0, location+1,  VIP_ENCINT(0), 0, tree )), tree );
				DO_ACCESS_INT(1, (temp*3) + VIP_ENCINT(1), VIP_CMOV(isNotDone && _isLeftMax, location+1, DO_ACCESS_INT(0, (temp*3) + VIP_ENCINT(1), 0, _tree )), _tree );
				DO_ACCESS_INT(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isLeftMax, elements[i], DO_ACCESS_INT(0, ((location+1) * 3) +  VIP_ENCINT(0), 0, _tree )), _tree );
				location = VIP_CMOV(isNotDone && _isLeftMax, location + 1, location);
				isMax_l = VIP_CMOV(isNotDone && _isLeftMax, VIP_ENCBOOL(true), isMax_l);

				dup = VIP_CMOV(isNotDone && notBoth, elements[i], dup);
				_result = VIP_CMOV(isNotDone && notBoth, VIP_ENCBOOL(false), _result);
				isMax_r = VIP_CMOV(isNotDone && !isMax_r && notBoth && !(_result), VIP_ENCBOOL(true), isMax_r);
				isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));

			}
			
		}
		return _result;

	#elif VIP_BUCKET_ACCESS	
		// cout<<"VIP_BUCKET_ACCESS"<<endl;
		VIP_ENCINT _tree [SIZE * 3];  // Flattened Version of 2D array
		for(int i = 0; i < SIZE;i++){
			// tree[i][0] = MAX;
			_tree[i * 3 + 0] = MAX;

			// tree[i][1] = MAX;
			_tree[i * 3 + 1] = MAX;

			// tree[i][2] = MAX;
			_tree[i * 3 + 2] = MAX;
		}	
		// tree[0][0] = elements[0];
		_tree[0] = elements[0];
		VIP_ENCINT location = 0;
		VIP_ENCBOOL _result = true;

		for(int i = 1; i < SIZE; i++){
			VIP_ENCINT temp = 0;
			// VIP_ENCINT value = DO_ACCESS_INT(0, 0, 0, tree[0] );
			// VIP_ENCINT left = DO_ACCESS_INT(0, 1, 0, tree[0] );
			// VIP_ENCINT right = DO_ACCESS_INT(0, 2, 0, tree[0] );
			VIP_ENCINT value = DO_ACCESS_INT(0, (0*3) + 0, 0, _tree );
			VIP_ENCINT left = DO_ACCESS_INT(0, (0*3) + 1, 0, _tree );
			VIP_ENCINT right = DO_ACCESS_INT(0, (0*3) + 2, 0, _tree );
			VIP_ENCBOOL isMax_r = false;
			VIP_ENCBOOL isMax_l = false;
			VIP_ENCBOOL isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));

			for( int k = 0; k < SIZE/2; k++){
				VIP_ENCBOOL condition1 = elements[i] > value;
				VIP_ENCBOOL condition2 = right != MAX;

				VIP_ENCBOOL _isRightNotMax = condition1 && condition2;
				VIP_ENCBOOL _isRightMax = condition1 && !condition2;

				VIP_ENCBOOL condition3 = elements[i] < value;
				VIP_ENCBOOL condition4 = left != MAX;

				VIP_ENCBOOL _isLeftNotMax = condition3 && condition4;
				VIP_ENCBOOL _isLeftMax = condition3 && !condition4;

				VIP_ENCBOOL notBoth = !condition1 && !condition3;

				// cout<<"isLeftNotMax: "<<VIP_DEC(_isLeftNotMax)<<" isLeftMax: "<<VIP_DEC(_isLeftMax) <<" isRightNotMax: "<<VIP_DEC(_isRightNotMax)<<" isRightMax: "<<VIP_DEC(_isRightMax)<<endl;
				// value = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right, VIP_ENCINT(0), 0, tree ), value);
				// left = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right,  VIP_ENCINT(1), 0, tree ), left);
				// temp = VIP_CMOV(isNotDone && _isRightNotMax, right, temp);
				// right = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT2D(0, right,  VIP_ENCINT(2), 0, tree ), right);

				value = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(0), 0, _tree ), value);
				left = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(1), 0, _tree ), left);
				temp = VIP_CMOV(isNotDone && _isRightNotMax, right, temp);
				right = VIP_CMOV(isNotDone && _isRightNotMax, DO_ACCESS_INT(0, (right*3) + VIP_ENCINT(2), 0, _tree ), right);

				// DO_ACCESS_INT2D(1, location+1,  VIP_ENCINT(0), VIP_CMOV(isNotDone && _isRightMax, elements[i], DO_ACCESS_INT2D(0, location+1,  VIP_ENCINT(0), 0, tree )) , tree );
				// DO_ACCESS_INT2D(1, temp,  VIP_ENCINT(2), VIP_CMOV(isNotDone && _isRightMax, location + 1, DO_ACCESS_INT2D(0, temp,  VIP_ENCINT(2), 0, tree )), tree );
				DO_ACCESS_INT(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isRightMax, elements[i], DO_ACCESS_INT(0, ((location+1)*3) +  VIP_ENCINT(0), 0, _tree )) , _tree );
				DO_ACCESS_INT(1, (temp*3) + VIP_ENCINT(2), VIP_CMOV(isNotDone && _isRightMax, location + 1, DO_ACCESS_INT(0, (temp*3) +  VIP_ENCINT(2), 0, _tree )), _tree );
				location = VIP_CMOV(isNotDone && _isRightMax, location + 1, location);
				isMax_r = VIP_CMOV(isNotDone && _isRightMax, VIP_ENCBOOL(true), isMax_r);
				
				// value = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT2D(0, left, VIP_ENCINT(0), 0, tree ), value);
				// right = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT2D(0, left, VIP_ENCINT(2), 0, tree ), right);
				value = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(0), 0, _tree ), value);
				right = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(2), 0, _tree ), right);
				temp = VIP_CMOV(isNotDone && _isLeftNotMax, left, temp);
				left = VIP_CMOV(isNotDone && _isLeftNotMax, DO_ACCESS_INT(0, (left*3) + VIP_ENCINT(1), 0, _tree ), left);
					
				// DO_ACCESS_INT2D(1, temp, VIP_ENCINT(1), VIP_CMOV(isNotDone && _isLeftMax, location+1, DO_ACCESS_INT2D(0, temp, VIP_ENCINT(1), 0, tree )), tree );
				// DO_ACCESS_INT2D(1, location+1,  VIP_ENCINT(0), VIP_CMOV(isNotDone && _isLeftMax, elements[i], DO_ACCESS_INT2D(0, location+1,  VIP_ENCINT(0), 0, tree )), tree );
				DO_ACCESS_INT(1, (temp*3) + VIP_ENCINT(1), VIP_CMOV(isNotDone && _isLeftMax, location+1, DO_ACCESS_INT(0, (temp*3) + VIP_ENCINT(1), 0, _tree )), _tree );
				DO_ACCESS_INT(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isLeftMax, elements[i], DO_ACCESS_INT(0, ((location+1) * 3) +  VIP_ENCINT(0), 0, _tree )), _tree );
				location = VIP_CMOV(isNotDone && _isLeftMax, location + 1, location);
				isMax_l = VIP_CMOV(isNotDone && _isLeftMax, VIP_ENCBOOL(true), isMax_l);

				dup = VIP_CMOV(isNotDone && notBoth, elements[i], dup);
				_result = VIP_CMOV(isNotDone && notBoth, VIP_ENCBOOL(false), _result);
				isMax_r = VIP_CMOV(isNotDone && !isMax_r && notBoth && !(_result), VIP_ENCBOOL(true), isMax_r);
				isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));
			}
			
		}
		return _result;
	
	#elif VIP_PATH_ACCESS	
		// cout<<"VIP_PATH_ACCESS"<<endl;
		VIP_ENCINT _tree [SIZE * 3];  // Flattened Version of 2D array
		for(int i = 0; i < SIZE;i++){
			// tree[i][0] = MAX;
			_tree[ i * 3 + 0] = MAX;
			// oram_tree->access(1, i * 3 + 0, MAX);

			// tree[i][1] = MAX;
			_tree[ i * 3 + 1] = MAX;
			// oram_tree->access(1, i * 3 + 1, MAX);

			// tree[i][2] = MAX;
			_tree[ i * 3 + 2] = MAX;
			// oram_tree->access(1, i * 3 + 2, MAX);
		}	
		// tree[0][0] = elements[0];
		// _tree[0] = elements[0];
		// oram_tree->access(1, 0 * 3 + 0, elements[0]);
		_tree[0] = elements[0]; // 25
		VIP_ENCINT location = 0;
		VIP_ENCBOOL _result = true;

		
		path_oram::btree* oram_tree = ORAM_INIT_INT(_tree, SIZE*3);
		// cout<<(oram_tree->access(0, 0, 0))<<endl;
		

		for(int i = 1; i < SIZE; i++){
			VIP_ENCINT temp = 0;
			// VIP_ENCINT value = DO_ACCESS_INT(0, 0, 0, tree[0] );
			// VIP_ENCINT left = DO_ACCESS_INT(0, 1, 0, tree[0] );
			// VIP_ENCINT right = DO_ACCESS_INT(0, 2, 0, tree[0] );
			VIP_ENCINT value = oram_tree->access(0, (0*3) + 0, 0); // 25
			VIP_ENCINT left = oram_tree->access(0, (0*3) + 1, 0); // MAX
			VIP_ENCINT right = oram_tree->access(0, (0*3) + 2, 0); // MAX
			VIP_ENCBOOL isMax_r = false;
			VIP_ENCBOOL isMax_l = false;
			// cout<<"start: "<<value<<" "<<" "<<left<<" "<<right<<endl;
			VIP_ENCBOOL isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));

			for( int k = 0; k < SIZE/2; k++){

				VIP_ENCBOOL condition1 = elements[i] > value; // 97 > 25 : True
				VIP_ENCBOOL condition2 = right != MAX; // false

				VIP_ENCBOOL _isRightNotMax = condition1 && condition2; 
				VIP_ENCBOOL _isRightMax = condition1 && !condition2;

				VIP_ENCBOOL condition3 = elements[i] < value;
				VIP_ENCBOOL condition4 = left != MAX;

				VIP_ENCBOOL _isLeftNotMax = condition3 && condition4;
				VIP_ENCBOOL _isLeftMax = condition3 && !condition4;

				VIP_ENCBOOL notBoth = !condition1 && !condition3;

				value = VIP_CMOV(isNotDone && _isRightNotMax, oram_tree->access(0, (right*3) + VIP_ENCINT(0), 0 ), value);
				left = VIP_CMOV(isNotDone && _isRightNotMax, oram_tree->access(0, (right*3) + VIP_ENCINT(1), 0), left);
				temp = VIP_CMOV(isNotDone && _isRightNotMax, right, temp);
				right = VIP_CMOV(isNotDone && _isRightNotMax, oram_tree->access(0, (right*3) + VIP_ENCINT(2), 0), right);
				
				
				oram_tree->access(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isRightMax, elements[i], oram_tree->access(0, ((location+1)*3) +  VIP_ENCINT(0), 0)));
				_tree[((location+1)*3) + VIP_ENCINT(0)] = VIP_CMOV(isNotDone && _isRightMax, elements[i], _tree[((location+1)*3) +  VIP_ENCINT(0)]);
				oram_tree->access(1, (temp*3) + VIP_ENCINT(2), VIP_CMOV(isNotDone && _isRightMax, location + 1, oram_tree->access(0, (temp*3) +  VIP_ENCINT(2), 0)));
				_tree[(temp*3) + VIP_ENCINT(2)] = VIP_CMOV(isNotDone && _isRightMax, location + 1, _tree[(temp*3) +  VIP_ENCINT(2)]);
				
				location = VIP_CMOV(isNotDone && _isRightMax, location + 1, location);
				isMax_r = VIP_CMOV(isNotDone && _isRightMax, VIP_ENCBOOL(true), isMax_r);
				value = VIP_CMOV(isNotDone && _isLeftNotMax, oram_tree->access(0, (left*3) + VIP_ENCINT(0), 0), value);
				right = VIP_CMOV(isNotDone && _isLeftNotMax, oram_tree->access(0, (left*3) + VIP_ENCINT(2), 0), right);
				temp = VIP_CMOV(isNotDone && _isLeftNotMax, left, temp);
				left = VIP_CMOV(isNotDone && _isLeftNotMax, oram_tree->access(0, (left*3) + VIP_ENCINT(1), 0), left);
				
				oram_tree->access(1, (temp*3) + VIP_ENCINT(1), VIP_CMOV(isNotDone && _isLeftMax, location+1, oram_tree->access(0, (temp*3) + VIP_ENCINT(1), 0)));
				_tree[(temp*3) + VIP_ENCINT(1)] = VIP_CMOV(isNotDone && _isLeftMax, location+1, _tree[(temp*3) + VIP_ENCINT(1)]);
				
				oram_tree->access(1, ((location+1)*3) + VIP_ENCINT(0), VIP_CMOV(isNotDone && _isLeftMax, elements[i], oram_tree->access(0, ((location+1) * 3) +  VIP_ENCINT(0), 0)));
				_tree[((location+1)*3) + VIP_ENCINT(0)] = VIP_CMOV(isNotDone && _isLeftMax, elements[i], _tree[((location+1) * 3) +  VIP_ENCINT(0)]);
				
				location = VIP_CMOV(isNotDone && _isLeftMax, location + 1, location);
				isMax_l = VIP_CMOV(isNotDone && _isLeftMax, VIP_ENCBOOL(true), isMax_l);
				dup = VIP_CMOV(isNotDone && notBoth, elements[i], dup);
				_result = VIP_CMOV(isNotDone && notBoth, VIP_ENCBOOL(false), _result);
				isMax_r = VIP_CMOV(isNotDone && !isMax_r && notBoth && !(_result), VIP_ENCBOOL(true), isMax_r);
				isNotDone = !((VIP_DEC(isMax_l) || VIP_DEC(isMax_r)) && !(VIP_DEC(isMax_l) && VIP_DEC(isMax_r)));
				
			}
			
		}
		return _result;
	#else
		// MEMORY ACCESS NOTE: This code is not hiding access at a certain index but doing an oblivious predication.
		cout<<"DEFAULT VIP_DO_MODE"<<endl;
		dup = MAX;

		for (int i=SIZE-1; i >= 0; i--)
		{
			for (int j=0; j < SIZE; j++)
			dup = VIP_CMOV(elements[i] == elements[j] && i!=j, elements[j], dup);
		}

		return (dup == MAX);
  	#endif

#else /* !VIP_DO_MODE */
//   cout<<"DEFAULT VIP_NATIVE_MODE"<<endl;
//    VIP_ENCINT tree[SIZE][3];
   dup = MAX;

   VIP_ENCINT _tree [SIZE * 3];
   for(int i = 0; i < SIZE;i++){
        // tree[i][0] = MAX;
		_tree[i * 3 + 0] = MAX;

		// tree[i][1] = MAX;
		_tree[i * 3 + 1] = MAX;

		// tree[i][2] = MAX;
		_tree[i * 3 + 2] = MAX;
	}	
	// tree[0][0] = elements[0];
	_tree[0] = elements[0];

	VIP_ENCINT location = 0;

	for(int i = 1; i < SIZE; i++){
		VIP_ENCINT temp = 0;
		// VIP_ENCINT value = tree[0][0];
		 VIP_ENCINT value = _tree[0*3 + 0];

		// VIP_ENCINT left = tree[0][1];
		VIP_ENCINT left = _tree[0*3 + 1];

		// VIP_ENCINT right = tree[0][2];
		VIP_ENCINT right = _tree[0*3 + 2];
		
		while(value != MAX){
			if(elements[i] > value){
				if(right != MAX){
					// value = tree[right][0];
					// left = tree[right][1];
					value = _tree[right*3 + 0];
					left = _tree[right*3 + 1];
					temp = right;
					// right = tree[right][2];
					right = _tree[right*3 + 2];
				}else{
					// tree[(location+1)*][0] = elements[i];
					// tree[temp][2] = location + 1;
					_tree[(location+1)*3 + 0] = elements[i];
					_tree[temp*3 + 2] = location + 1;
					location++;
					break;
				}
			}else if(elements[i] < value){
				if(left != MAX){
					// value = tree[left][0];
					// right = tree[left][2];
					value = _tree[left*3 + 0];
					right = _tree[left*3 + 2];

					temp = left;
					// left = tree[left][1];
					left = _tree[left*3 + 1];
				}else{
					_tree[temp*3 + 1] = location+1;
					_tree[(location+1) * 3 + 0] = elements[i];
					// tree[temp][1] = location+1;
					// tree[(location+1)][0] = elements[i];
					location++;
					break;
				}
			}else{
        		dup = elements[i];
				return false;
			}
		}
	}
	return true;
#endif /* VIP_DO_MODE */
}

int
main(void)
{
	{
		

		VIP_ENCINT dup1, dup2;
		VIP_ENCBOOL condition1, condition2;
		{
			Stopwatch start("VIP_BENCH_RUN_TIME");
			for(int i = 0; i<100; i++){
				condition1 = isDistinct(elements1, dup1);
				condition2 = isDistinct(elements2, dup2);
			}
			
		}
		if (VIP_DEC(condition1))
				cout<<"The elements of `elements1' are distinct"<<endl;
			else
				cout<<"The elements of `elements1' are not distinct (e.g., " << VIP_DEC(dup1) << " is duplicated)"<<endl;
			
		if (VIP_DEC(condition2))
				cout<<"The elements of `elements2' are distinct"<<endl;
			else
				cout<<"The elements of `elements2' are not distinct (e.g., " << VIP_DEC(dup2) << " is duplicated)"<<endl;
	}

	return 0;
}

