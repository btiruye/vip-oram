#ifndef BUCKET_ORAM_H
#define BUCKET_ORAM_H

#include <iostream>
#include <cmath>
#include "../../../config.h"
#include "../../vip-functional-library/vip-functional-library.h"

using namespace std;

// extern inline VIP_ENCBOOL bkt_access(int op, VIP_ENCINT idx, VIP_ENCBOOL data, VIP_ENCBOOL arr[], int arr_size){
//     cout << "IN BUCKET ACCESS CALL"<<endl;
//     VIP_ENCBOOL _value = false;
//     // int N = DATASET_SIZE;
//     int k = 8;
//     int seg_size = ceil(arr_size / k);
//     int min_idx = floor(VIP_DEC(idx) / seg_size) * seg_size;
    
//     int upper_bound = ((floor(VIP_DEC(idx) / seg_size) + 1) * seg_size) - 1;
//     int max_idx = upper_bound > arr_size - 1 ? arr_size - 1 : upper_bound;

//     if(op == 0){ // read
//         for (int i = min_idx; i <= max_idx; i++)
//         {
//             _value = VIP_CMOV(i == idx, arr[i], _value);
//         }
//     }else if(op == 1){ // write
//         for(int i = 0; i < arr_size; i++){
//             arr[i] = VIP_CMOV(i == idx, data, arr[i]);
//             _value = VIP_CMOV(!(_value) && i == idx, VIP_ENCBOOL(true), _value);
//         }
//     }

//     return _value;
// }

#include <iostream>
#include <cmath>
using namespace std;

namespace bucket_oram{
    struct Bound
    {
        int min;
        int max;
    };
    class bucket_oram{
        
        int min_idx;
        int max_idx;
        int rows;
        int seg;
        VIP_ENCINT idx;

        private:
            static bucket_oram* instance;
            bucket_oram(VIP_ENCINT idx, int rows, int seg) {
                this->idx = idx;
                this->rows = rows;
                this->seg = (seg > 0 && seg < rows) ? seg : ceil(rows/2);

            }
            void setBounds(){
                int k =  seg;
                int seg_size = ceil( (int)rows / k);
                this->min_idx = floor(VIP_DEC(idx / seg_size)) * seg_size;
                
                int upper_bound = ((floor(VIP_DEC(idx) / seg_size) + 1) * seg_size) - 1;
                this->max_idx = upper_bound >  (int)rows - 1 ?  (int)rows - 1 : upper_bound;
            }

        public:
            // NOTE: This operation is going to take place in the secure hardware component.
            void setParams(VIP_ENCINT idx, int row, int seg){
                this->idx = idx;
                this->rows = row;
                this->seg = (seg > 0 && seg < row) ? seg : ceil(row/2);
                instance->setBounds();
            }

            static bucket_oram* getInstance(VIP_ENCINT idx, int row, int seg) {
                if (instance == NULL){
                    instance = new bucket_oram(idx, row, seg);
                    instance->setBounds();
                }  
                else{
                    if(VIP_DEC(instance->idx != idx) || instance->rows != row || instance->seg != seg){
                        instance->setParams(idx, row, seg);
                    }
                        
                }
                return instance;
            }

            
            Bound getBounds() {
                return {min_idx, max_idx};
            }
};
        

bucket_oram* bucket_oram::instance = (instance!=NULL) ? instance : NULL;


extern inline bucket_oram* oram_init(VIP_ENCINT idx, int rows, int segments = 0){
    bucket_oram* bkt_oram = bucket_oram::getInstance(idx, (int)rows, segments);
    return bkt_oram;
}

template <size_t rows>
extern inline VIP_ENCBOOL bkt_access_bool(int op, VIP_ENCINT idx, VIP_ENCBOOL data, VIP_ENCBOOL (&arr)[rows]){
    // cout << "TYPE BOOL:: IN BUCKET ACCESS CALL"<<endl;

    bucket_oram* bkt_oram = oram_init(idx, rows);
    VIP_ENCBOOL _value = false;
    int min_idx = (bkt_oram->getBounds()).min;

    int max_idx = (bkt_oram->getBounds()).max;

    if (op == 0)
    { // read
        for (int i = min_idx; i <= max_idx; i++)
        {
            _value = VIP_CMOV(i == idx, arr[i], _value);
        }
    }else if(op == 1){ // write
        for(int i = min_idx; i <=  max_idx; i++){
            arr[i] = VIP_CMOV(i == idx, data, arr[i]);
            _value = VIP_CMOV(!(_value) && i == idx, VIP_ENCBOOL(true), _value);
        }
    }

    return _value;
}
template <size_t rows>
extern inline VIP_ENCINT bkt_access_int(int op, VIP_ENCINT idx, VIP_ENCINT data, VIP_ENCINT (&arr)[rows]){
    // cout << "TYPE INT:: IN BUCKET ACCESS CALL "<<endl;
    VIP_ENCINT _value = -1;

    // Can change the segment size here
    bucket_oram* bkt_oram = oram_init(idx, rows, 8);
    int min_idx = (bkt_oram->getBounds()).min;
    int max_idx = (bkt_oram->getBounds()).max;

    // int k = ceil(rows/2);
    // int seg_size = ceil( rows / k);
    // int min_idx = floor(VIP_DEC(idx) / seg_size) * seg_size;
    // int upper_bound = ((floor(VIP_DEC(idx) / seg_size) + 1) * seg_size) - 1;
    // int max_idx = upper_bound >  (int)rows - 1 ?  (int)rows - 1 : upper_bound;

    if(op == 0){ // read
        for (int i = min_idx; i <= max_idx; i++)
        {
            _value = VIP_CMOV(i == idx, arr[i], _value);
        }
    }else if(op == 1){ // write
        for(int i = min_idx; i <= max_idx; i++){
            arr[i] = VIP_CMOV(i == idx, data, arr[i]);
            _value = VIP_CMOV(_value == -1 && i == idx, VIP_ENCINT(0), _value);
        }
    }
    return _value;
}
template <size_t rows, size_t cols>
extern inline VIP_ENCINT bkt_access_int2D(int op, VIP_ENCINT idx1, VIP_ENCINT idx2, VIP_ENCINT data, VIP_ENCINT (&arr)[rows][cols]){
    // cout << "TYPE INT2D:: IN BUCKET ACCESS CALL"<<endl;
    VIP_ENCINT _value = -1;

    bucket_oram* bkt_oram1 = oram_init(idx1, rows);
    int min_idx1 = (bkt_oram1->getBounds()).min;
    int max_idx1 = (bkt_oram1->getBounds()).max;

    bucket_oram* bkt_oram2 = oram_init(idx2, cols);
    int min_idx2 = (bkt_oram2->getBounds()).min;
    int max_idx2 = (bkt_oram2->getBounds()).max;

    if(op == 0){ // read
        for (int i = min_idx1; i <= max_idx1; i++)
        {
            for (int j = min_idx2; j <= max_idx2; j++)
            {
                _value = VIP_CMOV(i == idx1 && j == idx2, arr[i][j], _value);
            }
            
        }
    }else if(op == 1){ // write
        for (int i = min_idx1; i <= max_idx1; i++)
        {
            for (int j = min_idx2; j <= max_idx2; j++)
            {
                arr[i][j] = VIP_CMOV(i == idx1 && j == idx2, data, arr[i][j]);
                _value = VIP_CMOV(_value == -1 && i == idx1 && j == idx2, VIP_ENCINT(0), _value);
            }
        }
    }
    return _value;
}
}
#endif
