#ifndef PATH_ORAM_BOOL_H
#define PATH_ORAM_BOOL_H

#include <iostream>
#include <cmath>
#include <limits.h>
#include <vector>
#include <algorithm>
#include <random> // std::default_random_engine
#include <chrono> // std::chrono::system_clock
#include <unordered_map>
#include <map>
#include "../../../config.h"
#include "../../vip-functional-library/vip-functional-library.h"

#define DUMMY_IDX -INT_MAX
#define DUMMY_VAL -INT_MAX
#define NODE_CAPACITY 4.0
#define Block_Bool tuple<VIP_ENCINT, VIP_ENCBOOL>
#define Node_Bool vector<Block_Bool>
#define Tree_Bool vector<Node_Bool>
using namespace std;

namespace path_oram_bool
{
    // For debugging and testing
    // ##############################################################################
    void printArr(int *arr, int L)
    {
        cout << "Array: ";
        for (int i = 0; i < L; i++)
        {
            cout << arr[i] << " ";
        }
        cout << endl;
    }

    void printMap(map<int, int> &m)
    {
        for (auto &item : m)
        {
            cout << item.first << " | ";
            cout << item.second << endl;
            cout << "--+--" << endl;
        }
    }
    void printVector(vector<int> path)
    {
        for (auto i : path)
            std::cout << i << ' ';
        cout << endl;
    }
    void printNode(Node_Bool node)
    {
        for (int i = 0; i < node.size(); i++)
        {

            Block_Bool blk = node[i];
            cout << "---+----------" << endl;
            cout << i << "  | [ " << get<0>(blk) << ": ";
            cout << get<1>(blk) << "]" << endl;
        }
        cout << endl;
    }

    void printTree(Tree_Bool tree)
    {
        for (int i = 0; i < tree.size(); i++)
        {
            Node_Bool node = tree[i];
            for (int stashPointer = 0; stashPointer < NODE_CAPACITY; stashPointer++)
            {
                Block_Bool blk = node[stashPointer];
                cout << "---+----------" << endl;
                cout << i << " - " << stashPointer << "  | [ " << get<0>(blk) << ": ";
                cout << get<1>(blk) << "]" << endl;
            }
            cout << "--------------" << endl
                 << endl;
        }
        cout << endl;
    }
    void print(int data)
    {
        cout << data << endl;
    }

    // ###############################################################################

    class btree
    {

    public:
        btree(VIP_ENCBOOL *data, int N);
        ~btree();
        int getHeight();
        int getTotalNodes();
        int getTotalLeaves();
        vector<int> getPath(int branch);
        VIP_ENCBOOL access_bool(VIP_ENCINT op, VIP_ENCINT block, VIP_ENCBOOL new_data);
        void printData();

    private:
        default_random_engine e;
        Tree_Bool tree;
        int inputSize;
        int height;
        map<VIP_ENCINT, VIP_ENCINT> pos_map;
        Node_Bool stash;
        int getParent(int node);
        int randomPath(int node);
        void initPositionMap();
    };

    // template <int listSize>
    btree::btree(VIP_ENCBOOL *data, int N)
    {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        // change 1 back to seed
        default_random_engine gen(seed);
        e = gen;
        inputSize = N;
        int totalNodes = getTotalNodes();

        height = floor(log(totalNodes) / log(2));

        // populate a big temp node with all the data and dummy data
        // cout << "nodes: " << totalNodes << endl;
        int totalBlocks = totalNodes * NODE_CAPACITY;
        Node_Bool tempNode;
        for (int i = 0; i < totalBlocks; i++)
        {
            if (i < inputSize)
            {
                Block_Bool blk = make_tuple(i, data[i]);
                tempNode.push_back(blk);
            }
            else
            {

                Block_Bool dummy_blk = make_tuple(DUMMY_IDX, false);
                tempNode.push_back(dummy_blk);
            }
        }

        std::shuffle(tempNode.begin(), tempNode.end(), e);

        // add data to tree
        for (int i = 0; i < totalNodes; i++)
        {
            Node_Bool node;
            for (int stashPointer = 0; stashPointer < NODE_CAPACITY; stashPointer++)
            {
                node.push_back(tempNode[i * NODE_CAPACITY + stashPointer]);
            }
            tree.push_back(node);
        }

        initPositionMap();
    }

    btree::~btree()
    {
        tree.clear();
    }

    int btree::getHeight()
    {
        return height;
    }

    int btree::getTotalNodes()
    {
        return pow(2, floor(log(ceil(inputSize / NODE_CAPACITY)) / log(2)) + 1) - 1;
    }

    int btree::getTotalLeaves()
    {
        return pow(2, height);
    }

    // finds a node's parent
    int btree::getParent(int node)
    {
        return floor((node - 1) / 2.0);
    }

    vector<int> btree::getPath(int leaf_idx)
    {

        int node_idx = pow(2, height) + leaf_idx - 1;

        vector<int> path(height + 1);
        for (int i = height; i >= 0; i--)
        {
            path[i] = node_idx;
            node_idx = getParent(node_idx);
        }
        return path;
    }

    int btree::randomPath(int node)
    {
        std::uniform_int_distribution<int> distr(0, 1);
        int rand = distr(e);
        int child1 = 2 * node + 1;
        int child2 = 2 * node + 2;
        int minLeafIdx = pow(2, height) - 1;
        // if leaf node, return leaf_idx
        if (node >= minLeafIdx)
        {

            return node - minLeafIdx;
        }
        else
        {
            if (rand == 0)
            {
                return randomPath(child1);
            }
            else
            {
                return randomPath(child2);
            }
        }
    }

    void btree::initPositionMap()
    {
        for (int i = 0; i < tree.size(); i++)
        {
            Node_Bool node = tree[i];
            for (int j = 0; j < NODE_CAPACITY; j++)
            {
                VIP_ENCINT blk_idx = get<0>(node[j]);
                // decrypt blk_idx

                if (blk_idx != -1)
                {
                    VIP_ENCINT leaf = randomPath(i);
                    pos_map[blk_idx] = leaf;
                }
            }
        }
    }

    // returns: 0 for write operation and  the value for a read operation
    VIP_ENCBOOL btree::access_bool(int op, int blk_idx, VIP_ENCBOOL new_data)
    {

        int noOfLeafs = getTotalLeaves();
        VIP_ENCBOOL returnData = false; // should be an encrypted data type
        std::uniform_int_distribution<int> distr(0, noOfLeafs - 1);

        int leaf_idx = pos_map[blk_idx];
        pos_map[blk_idx] = distr(e);

        vector<int> path = getPath(leaf_idx);

        for (int i = 0; i <= height; i++)
        {
            int node_idx = path[i];
            Node_Bool node = tree[node_idx];
            for (int stashPointer = 0; stashPointer < NODE_CAPACITY; stashPointer++)
            {
                stash.push_back(node[stashPointer]);
            }
        }
        for (int i = 0; i < stash.size(); i++)
        {
            int stash_blk_idx = get<0>(stash[i]);

            if (stash_blk_idx == blk_idx)
            {
                if (op == 0)
                {
                    returnData = get<1>(stash[i]);
                }
                else if (op == 1)
                {
                    get<1>(stash[i]) = new_data;
                    returnData = true;
                }
            }
        }

        for (int i = height; i >= 0; i--)
        // for each node
        {
            int node = path[i];

            int capacity = NODE_CAPACITY;
            int stashPointer = 0;

            while (stashPointer < stash.size() && capacity > 0)
            // for each block in node
            {

                int stash_blk_idx = get<0>(stash[stashPointer]);
                // do read-write here

                if (stash_blk_idx == DUMMY_IDX)
                // if dummy : erase
                {

                    stash.erase(stash.begin() + stashPointer);
                }
                else
                {
                    int leaf_idx = pos_map[stash_blk_idx];

                    vector<int> stash_blk_path = getPath(leaf_idx);
                    if (node == stash_blk_path[i])
                    {

                        tree[node][NODE_CAPACITY - capacity] = (stash[stashPointer]);
                        stash.erase(stash.begin() + stashPointer);
                        capacity--;
                    }
                    else
                    {
                        stashPointer++;
                    }
                }
            }
            for (int i = 0; i < capacity; i++)
            {
                Block_Bool dummy_blk = make_tuple(DUMMY_IDX, false);
                tree[node][NODE_CAPACITY - capacity + i] = dummy_blk;
            }
        }

        return returnData;
    }
    extern btree *init_bool(bool *userData, int N)
    {

        btree *tree = new btree(userData, N);
        return tree;
    }
}

#endif