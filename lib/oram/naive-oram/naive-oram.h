#ifndef NAIVE_ORAM_H
#define NAIVE_ORAM_H

#include <iostream>
#include "../../../config.h"
#include "../../vip-functional-library/vip-functional-library.h"

using namespace std;
using namespace enc_lib;

namespace naive_oram{



// template <size_t rows>
// extern inline VIP_ENCBOOL access_bool(int op, VIP_ENCINT idx, VIP_ENCBOOL data, VIP_ENCBOOL (&arr)[rows]);
// template <size_t rows>
// extern inline VIP_ENCINT access_int(int op, VIP_ENCINT idx, VIP_ENCINT data, VIP_ENCINT (&arr)[rows]);
// template <size_t rows, size_t cols>
// extern inline VIP_ENCINT access_int2D(int op, VIP_ENCINT idx1, VIP_ENCINT idx2, VIP_ENCINT data, VIP_ENCINT (&arr)[rows][cols]);

// }

template <size_t rows>
extern inline VIP_ENCBOOL access_bool(int op, VIP_ENCINT idx, VIP_ENCBOOL data, VIP_ENCBOOL (&arr)[rows]){
    // cout << "TYPE BOOL:: IN NAIVE ACCESS CALL"<<endl;
    VIP_ENCBOOL _value = false;
    if(op == 0){ // read
        for(int i = 0; i < (int)rows; i++){
            _value = VIP_CMOV(i == idx, arr[i], _value);
        }
        
    }else if(op == 1){ // write
        for(int i = 0; i < (int)rows; i++){
            arr[i] = VIP_CMOV(i == idx, data, arr[i]);
            _value = VIP_CMOV(!(_value) && i == idx, VIP_ENCBOOL(true), _value);
        }
    }
    return _value;
}
template <size_t rows>
extern inline VIP_ENCINT access_int(int op, VIP_ENCINT idx, VIP_ENCINT data, VIP_ENCINT (&arr)[rows]){
    // cout << "TYPE INT:: IN NAIVE ACCESS CALL"<<endl;
    VIP_ENCINT _value = -1;
    if(op == 0){ // read
        for(int i = 0; i < (int)rows; i++){
            _value = VIP_CMOV(i == idx, arr[i], _value);
        }
        
    }else if(op == 1){ // write
        for(int i = 0; i < (int)rows; i++){
            arr[i] = VIP_CMOV(i == idx, data, arr[i]);
            _value = VIP_CMOV(_value == -1 && i == idx, VIP_ENCINT(0), _value);
        }
    }
    return _value;
}
template <size_t rows, size_t cols>
extern inline VIP_ENCINT access_int2D(int op, VIP_ENCINT idx1, VIP_ENCINT idx2, VIP_ENCINT data, VIP_ENCINT (&arr)[rows][cols]){
    VIP_ENCINT _value = -1;
    if(op == 0){ // read
        for(int i = 0; i < (int)rows; i++){
            for(int j = 0; j < (int)cols; j++){
                _value = VIP_CMOV(i == idx1 && j == idx2, arr[i][j], _value);

            }
        }
        
    }else if(op == 1){ // write
        for(int i = 0; i < (int)rows; i++){
            for(int j = 0; j < (int)cols; j++){
                arr[i][j] = VIP_CMOV(i == idx1 && j == idx2, data, arr[i][j]);
                _value = VIP_CMOV(_value == -1 && i == idx1 && j == idx2, VIP_ENCINT(0), _value);
            }
        }
    }
    return _value;
}
}

#endif