#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utils.h"
#include <iostream>
#include <bitset>

// include build configuration defines
#include "../config.h"

using namespace std;

#define DATASET_SIZE 100
// -1 for random, 0 for fixed, 1 for sequential, and > 1 for strided access
#define IDX_STRIDE 1
#define COMPUTATION_DEPTH 800000
// time(NULL) for time based random
#define SEED 16
VIP_ENCINT data[DATASET_SIZE];

VIP_ENCINT lfsr(VIP_ENCINT data)
{
  VIP_ENCINT lfsr = data;
  unsigned period = 0;

  for (int i = 0; i < COMPUTATION_DEPTH; i++)
  {
    VIP_ENCINT lsb = lfsr & 1; /* Get LSB (i.e., the output bit). */
    lfsr >>= 1;                /* Shift register */
    if (lsb)
    { /* If the output bit is 1, apply toggle mask. */
      lfsr ^= 0xB400u;
    }
    ++period;
  }

  return lfsr;
}

int main(void)
{
  // initialize the privacy enhanced execution target
  VIP_INIT;

  // initialize the pseudo-RNG
  mysrand(42);
  unsigned count = 0;

  // initialize array with random values
  for (unsigned i = 0; i < DATASET_SIZE; i++)
    data[i] = myrand();

  if (IDX_STRIDE == -1)
  {
    cout << "[VIP] Random access\n";

    Stopwatch s("VIP_Bench Runtime");
    for (int i = 0; i < DATASET_SIZE; i++)
    {
      srand(SEED);
      VIP_ENCINT idx = rand() % DATASET_SIZE;
      VIP_ENCINT res = lfsr(data[idx]);
      // write back
      data[idx] = res;
      count++;
    }
  }
  else if (IDX_STRIDE == 0)
  {
    cout << "[VIP] Fixed access\n";
    Stopwatch s("VIP_Bench Runtime");
    VIP_ENCINT idx = rand() % DATASET_SIZE;
    for (int i = 0; i < DATASET_SIZE; i++)
    {
      VIP_ENCINT res = lfsr(data[idx]);
      // write back
      data[idx] = res;
      count++;
    }
  }
  else
  {
    if (IDX_STRIDE == 1)
    {
      cout << "[VIP] Sequential access\n";
    }
    else if (IDX_STRIDE < -1)
    {
      cout << "[VIP] Invalid index stride\n";
      return 1;
    }
    else
    {
      cout << "[VIP] Strided access: " << IDX_STRIDE << "\n";
    }
    Stopwatch s("VIP_Bench Runtime");
    for (int i = 0; i < IDX_STRIDE; i++)
    {
      VIP_ENCINT idx = 0 + i;
      while (idx < DATASET_SIZE)
      {
        VIP_ENCINT res = lfsr(data[idx]);
        // write back
        data[idx] = res;
        count++;
        idx += IDX_STRIDE;
      }
    }
  }
  cout << "[VIP] values accessed: " << count << "\n";
  cout << "[VIP] computation depth: " << COMPUTATION_DEPTH << "\n";
  cout << "[VIP] total computations: " << COMPUTATION_DEPTH * count << "\n";
  return 0;
}
