#include <iostream>
#include <iomanip>
using namespace std;
#include "../config.h"

#define DATASET_SIZE 1000000
VIP_ENCINT data[DATASET_SIZE];
VIP_ENCINT enc_idx = 137;


#ifdef VIP_DO_MODE
// VIP_ENCBOOL  VIP_ORAM_INIT(VIP_ENCINT Array[]){
//   //do init
//   return 0;
// }
// VIP_ENCINT VIP_ORAM_ACCESS(VIP_ENCBOOL op, VIP_ENCINT idx, VIP_ENCINT data?){
  
// }

VIP_ENCINT get_n_ORAM(VIP_ENCINT array[], VIP_ENCINT enc_idx)
{
  VIP_ENCINT ret = 100;
  for (int i = 0; i < DATASET_SIZE; i++)
  {
    VIP_ENCBOOL _istarget = (i == enc_idx);
    ret = VIP_CMOV(_istarget, array[i], ret);
  }

  return ret;
}
int set_n_ORAM(VIP_ENCINT data, VIP_ENCINT array[], VIP_ENCINT enc_idx)
{
  int status = 0;
  for (int i = 0; i < DATASET_SIZE; i++)
  {
    VIP_ENCBOOL _istarget = (i == enc_idx);
    array[i] = VIP_CMOV(_istarget, data, array[i]);
  }

  return status;
}

VIP_ENCINT get_bucket_ORAM(VIP_ENCINT array[], VIP_ENCINT enc_idx)
{
  int N = DATASET_SIZE;
  int k = 8;
  int seg_size = std::ceil(N / k);
  int min_idx = std::floor(enc_idx / seg_size) * seg_size;
  int upper_bound = ((std::floor(enc_idx / seg_size) + 1) * seg_size) - 1;
  int max_idx = upper_bound > N - 1 ? N - 1 : upper_bound;

  VIP_ENCINT ret = 100;
  for (int i = min_idx; i <= max_idx; i++)
  {
    VIP_ENCBOOL _istarget = (i == enc_idx);
    ret = VIP_CMOV(_istarget, array[i], ret);
  }

  return ret;
}
int set_bucket_ORAM(VIP_ENCINT data, VIP_ENCINT array[], VIP_ENCINT enc_idx)
{
  int N = DATASET_SIZE;
  int k = 8;
  int seg_size = std::ceil(N / k);
  int min_idx = std::floor(enc_idx / seg_size) * seg_size;
  int upper_bound = ((std::floor(enc_idx / seg_size) + 1) * seg_size) - 1;
  int max_idx = upper_bound > N - 1 ? N - 1 : upper_bound;

  int status = 0;
  for (int i = min_idx; i <= max_idx; i++)
  {
    VIP_ENCBOOL _istarget = (i == enc_idx);
    array[i] = VIP_CMOV(_istarget, data, array[i]);
  }

  return status;
}
#endif





void access()
{
#ifdef VIP_DO_MODE


  // using naive oram
  VIP_ENCINT target = get_n_ORAM(data, enc_idx);
  std::cerr
      << "Result: " << target << "\n";
  set_n_ORAM(90, data, enc_idx);
  std::cerr
      << "Result: " << get_n_ORAM(data, enc_idx) << "\n";

  // using bucket oram
  // VIP_ENCINT target = get_bucket_ORAM(data, enc_idx);
  // std::cerr
  //     << "Result: " << target << "\n";
  // std::cerr
  //     << "Result: " << get_bucket_ORAM(data, enc_idx) << "\n";

#else /* !VIP_DO_MODE */
  VIP_ENCINT target = data[enc_idx];
  // fprintf(stderr, "Result: ", target);
  std::cerr << "Result: " << target << "\n";
  data[enc_idx] = 90;
  std::cerr << "Result: " << data[enc_idx] << "\n";

#endif /* VIP_DO_MODE */
}

int main()
{
  VIP_INIT;

  for (int i = 0; i < DATASET_SIZE; i++)
    data[i] = i;

  {
    Stopwatch s("VIP_Bench Runtime");
    access();
  }
  return 0;
}


