#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
using namespace std;

#include "utils.h"

// include build configuration defines
#include "../config.h"

#define MAX_SETVAL 5000
#define SETA_SIZE 500
#define SETB_SIZE 1000

void set_init(vector<VIP_ENCINT> &set)
{
    vector<int> _set(set.size());

    for (unsigned i = 0; i < _set.size(); i++)
    {
    redo:
        int val = myrand() % MAX_SETVAL;
        for (unsigned j = 0; i != 0 && j < i; j++)
        {
            if (_set[j] == val)
                goto redo;
        }
        set[i] = _set[i] = val;
    }
}

void set_print(const char *name, vector<VIP_ENCINT> &set)
{
    fprintf(stdout, "%s:\n", name);
    for (unsigned i = 0; i < set.size(); i++)
        fprintf(stdout, "  %s[%u] = %d\n", name, i, VIP_DEC(set[i]));
}

void set_intersect(vector<VIP_ENCINT> &setA, vector<VIP_ENCINT> &setB, vector<VIP_ENCBOOL> &setA_match)
{

    VIP_ENCINT arrA[SETA_SIZE];
    std::copy(setA.begin(), setA.end(), arrA);

    VIP_ENCINT arrB[SETB_SIZE];
    std::copy(setB.begin(), setB.end(), arrB);

#ifdef VIP_DO_MODE
#ifdef VIP_NAIVE_ACCESS
    // cout << "VIP_NAIVE_ACCESS" << endl;

    for (unsigned i = 0; i < setA.size(); i++)
    {
        VIP_ENCINT a = DO_ACCESS_INT(0, i, 0, arrA);
        VIP_ENCBOOL match = false;
        for (unsigned j = 0; j < setB.size(); j++)
        {
            VIP_ENCINT b = DO_ACCESS_INT(0, j, 0, arrB);

            match = VIP_CMOV(a == b, (VIP_ENCBOOL) true, match);
        }
        setA_match[i] = match;
    }
#elif VIP_BUCKET_ACCESS
    // cout << "VIP_Bucket_ACCESS" << endl;

    for (unsigned i = 0; i < setA.size(); i++)
    {
        VIP_ENCINT a = DO_ACCESS_INT(0, i, 0, arrA);
        VIP_ENCBOOL match = false;
        for (unsigned j = 0; j < setB.size(); j++)
        {
            VIP_ENCINT b = DO_ACCESS_INT(0, j, 0, arrB);

            match = VIP_CMOV(a == b, (VIP_ENCBOOL) true, match);
        }
        setA_match[i] = match;
    }
#elif VIP_PATH_ACCESS
    // cout << "VIP_PATH_ACCESS" << endl;
    path_oram::btree *setA_tree = ORAM_INIT_INT(arrA, SETA_SIZE);
    path_oram::btree *setB_tree = ORAM_INIT_INT(arrB, SETB_SIZE);

    for (unsigned i = 0; i < setA.size(); i++)
    {
        VIP_ENCINT a = setA_tree->access(0, i, 0);
        VIP_ENCBOOL match = false;
        for (unsigned j = 0; j < setB.size(); j++)
        {
            VIP_ENCINT b = setB_tree->access(0, j, 0);

            match = VIP_CMOV(a == b, (VIP_ENCBOOL) true, match);
        }
        setA_match[i] = match;
    }
#else
    for (unsigned i = 0; i < setA.size(); i++)
    {
        VIP_ENCBOOL match = false;
        for (unsigned j = 0; j < setB.size(); j++)
        {
            match = VIP_CMOV(setA[i] == setB[j], (VIP_ENCBOOL) true, match);
        }
        setA_match[i] = match;
    }
#endif
#else /* !VIP_DO_MODE */
    for (unsigned i = 0; i < setA.size(); i++)
    {
        VIP_ENCBOOL match = false;
        for (unsigned j = 0; j < setB.size(); j++)
        {
            if (setA[i] == setB[j])
                match = true;
        }
        setA_match[i] = match;
    }
#endif
}

int main(void)
{
    vector<VIP_ENCINT> setA(SETA_SIZE);
    vector<VIP_ENCINT> setB(SETB_SIZE);
    vector<VIP_ENCBOOL> setA_match(SETA_SIZE);

    // initialize the privacy enhanced execution target
    VIP_INIT;

    // initialize the pseudo-RNG
    mysrand(42);

    // initialize the set vectors
    set_init(setA);
    // set_print("setA", setA);
    set_init(setB);
    // set_print("setB", setB);

    {
        Stopwatch s("VIP_Bench Runtime");
        for (int i = 0; i < 100; i++)
        {
            set_intersect(setA, setB, setA_match);
        }
    }

    // print the intersection
    fprintf(stdout, "setA & setB:\n");

    unsigned j = 0;
    for (unsigned i = 0; i < setA_match.size(); i++)
    {
        if (VIP_DEC(setA_match[i]))
        {
            j++;
            // fprintf(stdout, "  setA_and_setB[%u] = %d\n", j, VIP_DEC(setA[i]));
        }
    }
    fprintf(stdout, "INFO: cardinality(setA & setB) == %u\n", j);
    return 0;
}
