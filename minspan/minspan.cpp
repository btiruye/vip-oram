#include<iostream>
#include<iomanip>
#include<climits>
#include<string>

#include "../config.h"
#include "utils.h"

using namespace std;

//
// Compute the minimal spanning tree of a randomly generated graph.
// This program implements Kruscal's algorithm (https://en.wikipedia.org/wiki/Kruskal%27s_algorithm)
// over a graph represented as an adjacency matrix (https://en.wikipedia.org/wiki/Adjacency_matrix).
// This algorithm works on graphs with multiple components that are not connected.
// 

// V represents the number of vertex in the graph G
const int V = 10;

// this are the names(representation) of each vertex
const std::string vertName[V] = {"Home","z-mall","st.pet","office","school","motel","restr.","library","airport","barber"};
#ifdef VIP_PATH_ACCESS
path_oram_bool::btree* path_oram_tree;
#endif

// find the vertex with min distance from the unknown vertexes
VIP_ENCINT
minVal(VIP_ENCINT dist[V], VIP_ENCBOOL (&known)[V])
{
	
	VIP_ENCINT distVal = INT_MAX;
	VIP_ENCINT min = -1;
			
#ifndef VIP_NA_MODE	
	
	VIP_ENCBOOL condition = false;

	// for(int i=0;i<V;i++){
	// 	condition = (distVal > dist[i]) && !known[i];
	// 	distVal = VIP_CMOV(condition, dist[i], distVal);
	// 	min = VIP_CMOV(condition, (VIP_ENCINT)i, min);					
	// }

	#ifdef VIP_NAIVE_ACCESS
		// cout<< "ACCESS TYPE: NAIVE ORAM"<<endl;
		for(int i=0;i<V;i++){
			condition = (distVal > dist[i]) && !known[i];
			distVal = VIP_CMOV(condition, dist[i], distVal);
			min = VIP_CMOV(condition, (VIP_ENCINT)i, min);					
		}
		DO_ACCESS_BOOL(1, min, (VIP_ENCBOOL)true, known);
	#elif VIP_BUCKET_ACCESS
		// cout<< "ACCESS TYPE: BUCKET ORAM"<<endl;
		for(int i=0;i<V;i++){
			condition = (distVal > dist[i]) && !known[i];
			distVal = VIP_CMOV(condition, dist[i], distVal);
			min = VIP_CMOV(condition, (VIP_ENCINT)i, min);					
		}
		DO_ACCESS_BOOL(1, min, (VIP_ENCBOOL)true, known);
	#elif VIP_PATH_ACCESS
		// cout<< "ACCESS TYPE: PATH ORAM"<<endl;
		for(int i=0;i<V;i++){
			condition = (distVal > dist[i]) && !(path_oram_tree->access_bool(0, i, 0));
			distVal = VIP_CMOV(condition, dist[i], distVal);
			min = VIP_CMOV(condition, (VIP_ENCINT)i, min);					
		}
		path_oram_tree->access_bool(1, min, (VIP_ENCBOOL)true);
	#else
		// cout<< "ACCESS TYPE: DEFAULT DO CODE"<<endl;
		
		for(int i=0;i<V;i++){
			condition = (distVal > dist[i]) && !known[i];
			distVal = VIP_CMOV(condition, dist[i], distVal);
			min = VIP_CMOV(condition, (VIP_ENCINT)i, min);					
		}

		for(int j=0;j<V;j++){
			known[j] = VIP_CMOV(min == j,VIP_ENCBOOL(true),known[j]);
		}
	#endif

#else	
	for (int i=0; i<V; i++)
  	{
		if (distVal>dist[i] && !known[i])
    	{
			distVal = dist[i];
			min = i;
		}					
	}
	known[min] = true;
#endif
  return min;
}

// find the shortest path from the source to all other vertexes
void
minSpanTree(VIP_ENCINT (&graph)[V][V], VIP_ENCINT path[V])
{
	VIP_ENCINT dist[V];

  // KNOWN[I] set to true when the algorithm has linked node I into the minimal spanning tree being built
	VIP_ENCBOOL known[V];
	VIP_ENCINT min = 0;

	VIP_ENCINT _graph[V*V]; // Flattened Version of 2D array
	for(int a=0;a<V;a++){
		for(int b=0;b<V;b++){
			_graph[(a * V) + b] = graph[a][b];

		}
	}
	
#ifndef VIP_NA_MODE

	VIP_ENCBOOL condition=false;

	//sets the source vertex as known and gives it a distance of 0;	
	/* MEMORY ACCESS NOTE: This code is not hiding memory access at some index but doing data-oblivious predication 
	  					   This code is executed with same complexity in both native and DO cases. */
	for(int i=0;i<V;i++){
		condition = (min == i);
		dist[i] = VIP_CMOV(condition,VIP_ENCINT(0),VIP_ENCINT(INT_MAX));
		known[i] = VIP_CMOV(condition,VIP_ENCBOOL(true),VIP_ENCBOOL(false));	
	}

	#ifdef VIP_NAIVE_ACCESS
		// cout<<"VIP_NAIVE_ACCESS"<<endl;
		for (int i=0;i<V;i++){
			for (int j = 0;j<V;j++){
				// VIP_ENCINT v = DO_ACCESS_INT2D(0, min, VIP_ENCINT(j), VIP_ENCINT(0), graph);
				VIP_ENCINT v = DO_ACCESS_INT(0, (min * V) + VIP_ENCINT(j), VIP_ENCINT(0), _graph);
				condition = !known[j] && (v != 0) && (v < dist[j]);
				dist[j] = VIP_CMOV(condition, v, dist[j]);
				path[j] = VIP_CMOV(condition, min, path[j]);				
			}
			min = minVal(dist, known);
		}
	#elif VIP_BUCKET_ACCESS
		// cout<<"VIP_BUCKET_ACCESS"<<endl;
		for (int i=0;i<V;i++){
			for (int j = 0;j<V;j++){
				VIP_ENCINT v = DO_ACCESS_INT(0, (min * V) + VIP_ENCINT(j), VIP_ENCINT(0), _graph);
				condition = !known[j] && (v != 0) && (v < dist[j]);
				dist[j] = VIP_CMOV(condition, v, dist[j]);
				path[j] = VIP_CMOV(condition, min, path[j]);				
			}
			min = minVal(dist, known);
		}
	#elif VIP_PATH_ACCESS
		// cout<<"VIP_PATH_ACCESS"<<endl;
		// VIP_ENCINT arr[V*V] = _graph;
		path_oram::btree* oram_tree = ORAM_INIT_INT(_graph, V*V);
		path_oram_tree = ORAM_INIT_BOOL(known, V);
		for (int i=0;i<V;i++){
			for (int j = 0;j<V;j++){
				VIP_ENCINT v = oram_tree->access(0, (min * V) + VIP_ENCINT(j), VIP_ENCINT(0));
				condition = !(path_oram_tree->access_bool(0, j, 0)) && (v != 0) && (v < dist[j]);
				dist[j] = VIP_CMOV(condition, v, dist[j]);
				path[j] = VIP_CMOV(condition, min, path[j]);				
			}
			min = minVal(dist, known);
		}

	#else
		// cout<<"DEFAULT VIP_DO_MODE"<<endl;
		for(int k = 0;k<V;k++){	
			for(int i=0;i<V;i++){
				for(int j=0;j<V;j++){
					
					//This states if the ith element is the min vertex from the unknowns and 
					//if it has a connection with element j(graph[i][j]!=0) and if the distance is smaller than
					//the previous distance then update the path and the distance					 
					condition = (min == i) && !known[j] && (_graph[i*V + j]!=0) && (_graph[i*V + j] < dist[j]);
					dist[j]=VIP_CMOV(condition,_graph[i*V + j],dist[j]);
					path[j]=VIP_CMOV(condition,min,path[j]);
				}
			}
			min = minVal(dist, known);
		}
	#endif


#else
	// cout<<"DEFAULT VIP NATIVE_MODE"<<endl;		
	for (int i=0; i<V; i++)
  {
		if (min == i)
    {
			dist[i] = 0;
			known[i] = true;
		}
    else
    {
			dist[i] = INT_MAX;
			known[i] = false;
		}
	}

	// TODO : check to see this code in comparison to its DO version
	for (int i=0;i<V;i++)
  	{
		for (int j = 0;j<V;j++)
    	{
			if (!known[j] && (_graph[min*V + j] != 0) && (_graph[min*V + j] < dist[j]))
      		{
				dist[j] = _graph[min*V + j];
				path[j] = min;				
			}
		}
		min = minVal(dist, known);
	}
#endif
}


//Used to initialize the graph
void initializeData(VIP_ENCINT graph[V][V])
{
	mysrand(10);
	for (int i=0; i < V; i++)
  	{
		for (int j=0;j<V;j++)
    	{
			if (i>j)
				graph[i][j] = graph[j][i];
			else if (i==j)
				graph[i][j] = 0;
			else
      		{
				if (myrand() % 5 == myrand() % 5)
					graph[i][j] = 0;	
				else
					graph[i][j] = myrand() % 10;			
			}
		}
	}
}

void
displayGraph(VIP_ENCINT graph[V][V])
{
	int index = 0;
	for (int i=-1;i<V;i++)
  	{
		for (int j=-1;j<V;j++)
    	{
			if (i==-1)
      		{
				if (j==-1)
					cout<<std::setw(7)<<" ";
				else
					cout<<std::setw(8)<<vertName[j];
			}
      		else
      		{
				if(j==-1)
        		{
					cout<<std::setw(8)<<vertName[index];
					index++;
				}
        		else
					cout<<std::setw(8)<<VIP_DEC(graph[i][j]);
			}
		}
    	cout<<endl;
	}
	cout<<endl<<endl;
}

void
displayGraph1(VIP_ENCINT graph[V][V], VIP_ENCINT path[V])
{
	int index = 0;
	for (int i=-1;i<V;i++)
  	{
		for (int j=-1;j<V;j++)
    {
			if (i==-1)
      {
				if (j==-1)
					cout<<std::setw(7)<<" ";
				else
					cout<<std::setw(8)<<vertName[j];
			}
      else
      {
				if(j==-1)
        {
					cout<<std::setw(8)<<vertName[index];
					index++;
				}
        else
					cout<<std::setw(8)<<VIP_DEC(graph[i][j]) << "/" << VIP_DEC(path[i]);
			}
		}
    cout<<endl;
	}
	cout<<endl<<endl;
}

//Displays the path from source to destination
void
displayPath(VIP_ENCINT source, VIP_ENCINT destination, VIP_ENCINT path[V])
{
	static int sourceF = VIP_DEC(source);
	static int destF = VIP_DEC(destination);
	static int count = 0;
	
	int currPath = VIP_DEC(destination);
	
	if(count == 0){
		cout<<"\nSource: "<<vertName[sourceF]<<"\tDestination: "<<vertName[currPath]<<endl<<endl;
		cout<<"Path: "<<vertName[sourceF];
		count++;
	}	
	if(VIP_DEC(path[currPath]) != sourceF){
		displayPath(sourceF,path[currPath],path);
	}
	cout<<"->"<<vertName[currPath];
	if(currPath == destF){
		cout<<endl;
	}
}

// display the minimum spanning tree
void
displayTree(VIP_ENCINT graph[V][V], VIP_ENCINT path[V])
{
  int cost = 0;
  fprintf(stdout, "minimum spanning tree:\n");
  for (int i=1; i < V; i++)
  {
    fprintf(stdout, "  %8s <-%d-> %8s\n", vertName[i].c_str(), VIP_DEC(graph[i][VIP_DEC(path[i])]), vertName[VIP_DEC(path[i])].c_str());
    cost += VIP_DEC(graph[i][VIP_DEC(path[i])]);
  }
  fprintf(stdout, "total cost = %d\n", cost);
}

int
main()
{
	// VIP_ENCINT source = 0;
	// VIP_ENCINT destination = 1;
	VIP_ENCINT graph[V][V];
	VIP_ENCINT path[V];	
	for(int i=0;i<V;i++){
		path[i]=-1;
	}
	
	initializeData(graph);	
	displayGraph(graph);
	{
		Stopwatch start("VIP_BENCH_RUN_TIME");
		for (int j=0; j<100; j++){
			minSpanTree(graph,path);
		}
		
	}
	// displayPath(source,destination,path);
	// displayGraph1(graph, path);
	displayTree(graph, path);
}

